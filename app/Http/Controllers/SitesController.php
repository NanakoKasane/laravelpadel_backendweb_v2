<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Site;
use Exception;
use App\Mail\NewSiteEmail;
use Illuminate\Support\Facades\Mail;

class SitesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$sites = Site::all();
        //$sites = Site::where('user_id', Auth::user()->id)->get();
        //$sites = Site::where('user_id', Auth::id())->get();
        $sites = Auth::user()->sites()->get();

        return view('sites.index', compact('sites'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sites.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $request->validate([
        //'name'=>'required|string|unique:sites|max:20',
        'name'=>'required|string|max:20',
        'link'=> 'required|url',
        'email' => 'required|email'
      ]);

      $site = new Site([
        'name' => $request->get('name'),
        'link'=> $request->get('link'),
        'email'=> $request->get('email'),
        //'user_id' => Auth::user()->id
        'user_id' => Auth::id()
      ]);
      $site->save();     
      $status = "success";
      $message = 'A new site ' . $site . ' has been added';



      $to = Auth::user()->email;
      try {
            Mail::to($to)->send(new NewSiteEmail($site));
            $message = $message . " and an email has been sent";
      } catch (Exception $exception) {
          $status = "error";
          $message = $message . ', but the email has not been sent. Error: ' . $exception->getMessage();              
        }



      return redirect('/sites')->with($status, $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $site = Site::findOrFail($id);

        return view('sites.edit', compact('site'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $request->validate([
        //'name'=>'required|string|unique:sites|max:20',
        'name'=>'required|string|max:20',
        'link'=> 'required|url',
        'email' => 'required|email'
      ]);

      $site = Site::findOrFail($id);
      $site->name = $request->get('name');
      $site->link = $request->get('link');
      $site->email = $request->get('email');
      $site->save();

      return redirect('/sites')->with('success', 'Site ' . $site->name . ' has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     $site = Site::find($id);
     $temp = $site->name;
     $site->delete();

     return redirect('/sites')->with('success', 'Site ' . $temp . ' has been deleted Successfully');
    }



}
