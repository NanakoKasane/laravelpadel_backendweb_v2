<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Site;
use Validator;

class ApiSitesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return response()->json(['sites' => auth()->user()->sites], 200);
        return response()->json(auth()->user()->sites, 200);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
                    'name' => 'required|unique:sites|max:20',
                    'link' => 'required|url',
                    'email' => 'required|email|max:100',
        ]);

        if ($validator->fails()) {
            $message = $validator->errors()->getMessages();
            $status = 422;
            //return response()->json( $validator->errors()->getMessages(), 422);
        } else {
            $site = new Site();
            $site->name = $request->name;
            $site->link = $request->link;
            $site->email = $request->email;

            if (auth()->user()->sites()->save($site)) {
                //return response()->json( $site, 201);
                $message = $site;
                $status = 201;
            } else {
                //return response()->json( 'El sitio no pudo ser añadido', 500);
                $message = 'El sitio no pudo ser añadido';
                $status = 500;
            }
        }

        return response()->json($message, $status);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Site $site)
    {
        $site = auth()->user()->sites()->find($site);

        if (!$site) {
            $message = 'Sitio no encontrado';
            $status = 404;
        } else {
            $message['site'] = $site;
            $status = 200;
        }

        return response()->json($message, $status); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Site $site)
    {
        $site = auth()->user()->sites()->find($site->id);

        if (!$site) {
            $message = 'Sitio no encontrado';
            $status = 404;
        } else {
            $validator = Validator::make($request->all(), [
                        'name' => 'required|unique:sites|max:20',
                        'link' => 'required|url',
                        'email' => 'required|email|max:100',
            ]);

            if ($validator->fails()) {
                $message = $validator->errors()->getMessages();
                $status = 422;
                //return response()->json( $validator->errors()->getMessages(), 422);
            } else {
                $updated = $site->update($request->all());
                //$updated = $site->fill($request->all())->save();

                if ($updated) {
                    $message = $site;
                    $status = 201;
                } else {
                    $message = 'El sitio ' . $site->name . ' no pudo ser actualizado';
                    $status = 500;
                }
            }
        }

        return response()->json($message, $status);     
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Site $site)
    {       
        $site = auth()->user()->sites()->find($site->id);

        if (!$site) {
            $message = 'Sitio no encontrado';
            $status = 404;
        } else {
            if ($site->delete()) {
                $message = null;
                $status = 204;
            } else {
                $message = 'El sitio no pudo ser eliminado';
                $status = 500;
            }
        }

        return response()->json($message, $status);
    }
}
