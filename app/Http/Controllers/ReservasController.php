<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Site;
use Exception;
use App\Mail\NewSiteEmail;
use Illuminate\Support\Facades\Mail;
use App\reservas;

class ReservasController extends Controller
{
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {		
		$reservas = Auth::user()->reservas()->get();

        return view('reservas.index', compact('reservas'));
    }


	public function indexsearchdate(Request $request){
       $request->validate([
        'fecha'=> 'required|date|after:tomorrow|date_format:Y-m-d'
      ]);

	$reservas = DB::table('reservas')
                     ->select(DB::raw('*'))
                     ->where('fecha', '=', $request -> get('fecha'))
                     ->get();

		//$message;
		//$status;
		if (empty($reservas[0])){
			echo "Esta fecha no tiene reservas";
			//$status = "error";
			//$message = "Este usuario no existe o no tiene reservas añadidas aún";
		}

        return view('reservas.index2', compact('reservas')); //->with($status, $message);
	}

	public function index2(Request $request){

		$reservas = DB::table('reservas')
                     ->select(DB::raw('*'))
                     ->where('user_id', '=', $request -> get('iduser'))
                     ->get();

		//$message;
		//$status;
		if (empty($reservas[0])){
			echo "Este usuario no existe o no tiene reservas añadidas aún";
			//$status = "error";
			//$message = "Este usuario no existe o no tiene reservas añadidas aún";
		}

        return view('reservas.index2', compact('reservas')); //->with($status, $message);
	}

	public function index3(Request $request){
	$users = DB::table('users')
		               ->select('id') //  DB::raw('')
		               ->where('name', $request -> get('name'));

	$reservas = DB::table('reservas')
		    ->joinSub($users, 'users', function ($join) {
		        $join->on('users.id', '=', 'reservas.user_id');
		    })->get();


		if (empty($reservas[0])){
			echo "Este usuario no existe o no tiene reservas añadidas aún";
		}

        return view('reservas.index2', compact('reservas'));
	}

	public function search(){

        return view('reservas.search');
	}

	public function searchname(){
		 return view('reservas.searchname');
	}

	public function searchdate(){
		 return view('reservas.searchdate');
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('reservas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       $request->validate([
        'numhoras'=>'required|max:2',
        'fecha'=> 'required|date|after:today|date_format:Y-m-d',
		'hora'=> 'required|date_format:H:i'
      ]);

$horatmp = $request->get('hora');
 $horatmp2 =  strtotime($horatmp) - 60*60;
$horatmp2 = date("h:i", $horatmp2);

 echo $horatmp;
 $horadespues =  strtotime($horatmp) + 60*60;

$horadespues = date("h:i", $horadespues);
echo $horadespues;
$fecha = $request->get('fecha');

	if ($request->get('numhoras') >= 1){
		$count = DB::select("select count(*) from reservas where fecha = '$fecha' and hora >= '$horatmp2' and hora < '$horadespues'");
print_r($count);

	if ($request->get('numhoras') == 2){
		 $horadespues =  strtotime($horatmp) + 60*60*2;
		$horadespues = date("h:i", $horadespues);
		$count = DB::select("select count(*) from reservas where fecha = '$fecha' and hora >= '$horatmp2' and hora < '$horadespues'");
	}

	$count = json_decode(json_encode($count), true);
		//$count = $count -> toArray();

echo $count[0]["count(*)"];
		if ($count[0]["count(*)"] == 0) { // -> "count(*)" > 0){
			echo "No solapa";
			  $reserva = new reservas([
				'numhoras' => $request->get('numhoras'),
				'fecha'=> $request->get('fecha'),
				'user_id' => Auth::id(),
				'hora'=> $request->get('hora')
			  ]);
			  $reserva->save();     
			  $status = "success";
			  $message = 'Una nueva reserva: ' . $reserva . ' se ha añadido';



			  $to = Auth::user()->email;
			  try {
				    Mail::to($to)->send(new NewSiteEmail($reserva));
				    $message = $message . " y el email se ha mandado";
			  } catch (Exception $exception) {
				  $status = "error";
				  $message = $message . ', pero no se ha enviado el email. Error: ' . $exception->getMessage();              
				}
		}
		else{
			$status = "error";
			$message = "En esta fecha y horas ya hay una reserva añadida";
		}
	}


	// if ($request->get('numhoras') >= 1)
	// select count(*) from reservas where fecha = $request->get('fecha'),
	// and hora >= $request->get('hora') and hora < strtotime('$request->get('hora')') + 60*60  ;

	// if $request->get('numhoras')== 2)
	// select count(*) from reservas where fecha = $request->get('fecha'),
	// and hora >= $request->get('hora') and hora < strtotime('$request->get('hora')') + 60*60*2  ;


/*
      $reserva = new reservas([
        'numhoras' => $request->get('numhoras'),
        'fecha'=> $request->get('fecha'),
        'user_id' => Auth::id(),
		'hora'=> $request->get('hora')
      ]);
      $reserva->save();     
      $status = "success";
      $message = 'Una nueva reserva: ' . $reserva . ' se ha añadido';



      $to = Auth::user()->email;
      try {
            Mail::to($to)->send(new NewSiteEmail($reserva));
            $message = $message . " y el email se ha mandado";
      } catch (Exception $exception) {
          $status = "error";
          $message = $message . ', pero no se ha enviado el email. Error: ' . $exception->getMessage();              
        }
*/
      return redirect('/reservas' )->with($status, $message);


	/**
	$numhoras = $request -> input('numhoras');	
	$fecha = $request -> input('fecha');
	$user_id = Auth::id();
	DB::insert('insert into reservas (numhoras, fecha) values (?, ?)', [$numhoras, $fecha]);
*/
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $reserva = reservas::findOrFail($id);
		echo $reserva -> id;

        return view('reservas.edit', compact('reserva'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

       $request->validate([
        'numhoras'=>'required|max:2',
        'fecha'=> 'required|date|after:today|date_format:Y-m-d',
		'hora'=> 'required|date_format:H:i'
      ]);

      $reserva = reservas::findOrFail($id);
      $reserva->fecha = $request->get('fecha');
	$reserva->hora = $request->get('hora');
      $reserva->numhoras = $request->get('numhoras');
      // $site->email = $request->get('email');


	$horatmp = $request->get('hora');
	 $horatmp2 =  strtotime($horatmp) - 60*60;
	$horatmp2 = date("h:i", $horatmp2);

	 echo $horatmp;
	 $horadespues =  strtotime($horatmp) + 60*60;

	$horadespues = date("h:i", $horadespues);
	echo $horadespues;
	$fecha = $request->get('fecha');

		if ($request->get('numhoras') >= 1){
			$count = DB::select("select count(*) from reservas where fecha = '$fecha' and hora >= '$horatmp2' and hora < '$horadespues'");
	print_r($count);

		if ($request->get('numhoras') == 2){
			 $horadespues =  strtotime($horatmp) + 60*60*2;
			$horadespues = date("h:i", $horadespues);
			$count = DB::select("select count(*) from reservas where fecha = '$fecha' and hora >= '$horatmp2' and hora < '$horadespues'");
		}

		$count = json_decode(json_encode($count), true);
			//$count = $count -> toArray();

	echo $count[0]["count(*)"];
			if ($count[0]["count(*)"] == 0) { // -> "count(*)" > 0){
				echo "No solapa";
					    $reserva->save();
			$status = "success";
			$message = 'La reserva ' . $reserva->id . ' se ha modificado';
			}
			else{
				$status = "error";
				$message = "En esta fecha y horas ya hay una reserva añadida";
			}
		}


      return redirect('/reservas')->with($status, $message);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     $reserva = reservas::find($id);
     $temp = $reserva->id;
     $reserva->delete();

     return redirect('/reservas')->with('success', 'La reserva ' . $temp . ' se ha anulado correctamente');
    }
}
