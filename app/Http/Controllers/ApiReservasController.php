<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Site;
use Exception;
use App\Mail\NewSiteEmail;
use Illuminate\Support\Facades\Mail;
use App\reservas;
use Validator;

class ApiReservasController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {		
		// $reservas = Auth::user()->reservas()->get();
       // return view('reservas.index', compact('reservas'));
		 return response()->json(auth()->user()->reservas, 200); 

    }

public function indexsearchdate(Request $request){
       //$request->validate([
       // 'fecha'=> 'required|date|after:tomorrow|date_format:Y-m-d'
      //]);

	$reservas = DB::table('reservas')
                     ->select(DB::raw('*'))
                     ->where('fecha', '=', $request -> get('fecha'))
                     ->get();

		$message = $reservas;
		$status = 200;

		if (empty($reservas[0])){
			$status = 404;
			$message = "Esta fecha no tiene reservas añadidas";
		}

		return response() -> json($message, $status);
      //  return view('reservas.index2', compact('reservas')); //->with($status, $message);
	}

	public function index2(Request $request){

		$reservas = DB::table('reservas')
                     ->select(DB::raw('*'))
                     ->where('user_id', '=', $request -> get('iduser'))
                     ->get();

		$message = $reservas;
		$status = 200;
		if (empty($reservas[0])){
			$status = 404;
			$message = "Este usuario no existe o no tiene reservas añadidas aún";
		}

		return response() -> json($message, $status);

        // return view('reservas.index2', compact('reservas')); //->with($status, $message);
	}

	public function index3(Request $request){
	$users = DB::table('users')
		               ->select('id') //  DB::raw('')
		               ->where('name', $request -> get('name'));

	$reservas = DB::table('reservas')
		    ->joinSub($users, 'users', function ($join) {
		        $join->on('users.id', '=', 'reservas.user_id');
		    })->get();


		$message = $reservas;
		$status = 200;
		if (empty($reservas[0])){
			$status = 404;
			$message = "Este usuario no existe o no tiene reservas añadidas aún";
		}

		return response() -> json($message, $status);

        // return view('reservas.index2', compact('reservas'));
	}

	public function search(){

        //return view('reservas.search');
	}

	public function searchname(){
		 // return view('reservas.searchname');
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return view('reservas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

		$validator = Validator::make($request -> all(),
		[
		    'numhoras'=>'required|max:2',
		    'fecha'=> 'required|date|date_format:Y-m-d',
			'hora'=> 'required|date_format:H:i'
		]);

		if ($validator -> fails()){
			$message = $validator -> errors -> getMessages();
			$status = 422;
		}
		else{
		      $reserva = new reservas([
				'numhoras' => $request->get('numhoras'),
				'fecha'=> $request->get('fecha'),
				'hora'=> $request->get('hora'),
				'user_id' => Auth::id()
      			]);
			if (auth()->user()->reservas()->save($reserva)){
				$message = $reserva;
				$status = 201;
			} 
			else{
				$message = "La reserva no pudo completarse";
				$status = 500;
			}
		}

      $to = Auth::user()->email;
      try {
            Mail::to($to)->send(new NewSiteEmail($reserva));
            // $message .= $message . " y el email se ha mandado";
      } catch (Exception $exception) {
          $status = 500;
          $message .= $message . ', pero no se ha enviado el email. Error: ' . $exception->getMessage();              
        }

      // return redirect('/reservas' )->with($status, $message);

	return response() -> json($message, $status);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(reservas $reserva)
    {
        $reserva = auth()->user()->reservas()->find($reserva);

        if (!$reserva) {
            $message = 'Reserva no encontrada';
            $status = 404;
        } else {
            $message['reserva'] = $reserva;
            $status = 200;
        }

        return response()->json($message, $status); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       //$reserva = reservas::findOrFail($id);
		// echo $reserva -> id;

        // return view('reservas.edit', compact('reserva'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, reservas $reserva) // Cambio id a reserva
    {

		$reserva = auth()->user()->reservas()->find($reserva->id);
		if (!$reserva){
			$message = "Reserva no encontrada";
			$status = 404;
		}
		else{
			$validator = Validator::make($request -> all(),
			[
				'numhoras'=>'required|max:2',
				'fecha'=> 'required|date|date_format:Y-m-d',
				'hora'=> 'required|date_format:H:i'
			]);

			if ($validator -> fails()){
				$message = $validator -> errors -> getMessages();
				$status = 422;
			}
			else{
				 // $reserva = new reservas([
				//	'numhoras' => $request->get('numhoras'),
				//	'fecha'=> $request->get('fecha'),
				//	'hora'=> $request->get('hora'),
				//	'user_id' => Auth::id()
		  		//	]);

/**
				if (auth()->user()->reservas()->save($reserva)){
					$message = $reserva;
					$status = 201;
				} 
				else{
					$message = "La reserva no pudo completarse";
					$status = 500;
				}
*/

                $updated = $reserva->update($request->all());
                //$updated = $reserva->fill($request->all())->save();

                if ($updated) {
                    $message = $reserva;
                    $status = 201;
                } else {
                    $message = 'La reserva ' . $reserva->name . ' no pudo ser modificada';
                    $status = 500;
                }



			}
		}

		return response()->json($message, $status);

/**
       $request->validate([
        'numhoras'=>'required|max:2',
        'fecha'=> 'required',
      ]);

      $reserva = reservas::findOrFail($id);
      $reserva->fecha = $request->get('fecha');
      $reserva->numhoras = $request->get('numhoras');
      // $site->email = $request->get('email');
      $reserva->save();

      return redirect('/reservas')->with('success', 'La reserva ' . $reserva->id . ' se ha modificado');
*/


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(reservas $reserva) // $id
    {
		$reserva = auth()->user()->reservas()->find($reserva->id);
		if (!$reserva){
			$message = "Reserva no encontrada";
			$status = 404;
		}
		else{
			if ($reserva->delete()){
				$message = null;
				$status = 204;
			}
			else{
				$message = "No pudo borrarse la reserva";
				$status = 500;
			}

		}
		
     	return response()->json($message, $status);
    }
}
