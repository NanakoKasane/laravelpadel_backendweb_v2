<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\reservas;

class NewSiteEmail extends Mailable
{
    use Queueable, SerializesModels;

    private $reserva;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(reservas $reserva)
    {
        $this->reserva = $reserva;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //return $this->view('view.name');
        return $this->view('emails.newsite')
                    ->with('reserva', $this->reserva);
                    //->from($address, $name)
                    //->cc($address, $name)
                    //->bcc($address, $name)
                    //->replyTo($address, $name)
                    //->subject($subject);
    }
}
