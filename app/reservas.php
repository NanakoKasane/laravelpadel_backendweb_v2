<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class reservas extends Model
{
    protected $fillable = ['numhoras', 'fecha', 'user_id', 'hora'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
