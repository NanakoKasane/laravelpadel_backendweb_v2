<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


//Route::middleware('auth:api')->get('/user', function (Request $request) {return $request->user();});


Route::post('/email', 'EmailController@send');

Route::post('login', 'PassportController@login');
//ruta: api/login
Route::post('register', 'PassportController@register');
//ruta: api/register

Route::middleware('auth:api')->group(function () {
    Route::get('user', 'PassportController@getDetails');
    //ruta: api/user
    Route::resource('sites', 'ApiSitesController');



	// AÑADO:
	Route::resource('reservas', 'ApiReservasController');
	// Route::get('reservas.search','ApiReservasController@search')  -> name('reservas.search');
	// Route::get('reservas.searchname','ApiReservasController@searchname')  -> name('reservas.searchname');

	 Route::post('reservas.index2','ApiReservasController@index2')  -> name('reservas.index2');

	 Route::post('reservas.index3','ApiReservasController@index3')  -> name('reservas.index3');

	 Route::get('reservas.index2','ApiReservasController@index2')  -> name('reservas.index2');

	 Route::get('reservas.index3','ApiReservasController@index3')  -> name('reservas.index3');


	 Route::get('reservas.indexsearchdate','ApiReservasController@indexsearchdate')  -> name('reservas.indexsearchdate');
	 Route::post('reservas.indexsearchdate','ApiReservasController@indexsearchdate')  -> name('reservas.indexsearchdate');


    //ruta: api/sites
    Route::get('logout', 'PassportController@logout');
    //ruta: api/logout
});

