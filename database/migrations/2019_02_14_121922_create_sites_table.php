<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

/**
        Schema::create('sites', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });
*/

 /* Añadir las columns for name, link, email y user_id */
    Schema::create('sites', function (Blueprint $table) {
        $table->increments('id');
        $table->timestamps();
        $table->string('name');
        $table->string('link');
        $table->string('email');
        $table->integer('user_id')->nullable();

    });

 
   }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sites');
    }
}
