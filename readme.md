<p align="center"><img align="center" src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<p>He realizado una aplicación web y una API con laravel para poder gestionar las reservas de una pista de padel de una urbanización. </p>
<p>Para ello he creado primero la estructura de la tabla de las reservas en database/migrations. </p>
<p>He decidido que la <b> tabla reservas </b> tenga un id, un número de horas que se querrá reservar la pista, una fecha de reserva, una hora del inicio de la reserva y el usuario que la reserva como clave foránea </p><br/>
<p>Una vez creada la tabla, creo el <b>modelo llamado "reservas"</b> y posteriormente el controlador <b>ReservasController</b>, en el que estará todo el funcionamiento. Y finalmente, añado las vistas y las rutas necesarias para su funcionamiento.</p>
<br/>

<p>Para acceder a las siguientes opciones explicadas a continuación solo hay que pulsar cada uno de los 5 botones que hay en el home: <i>https://laravel.marina.edufdezsoy.es/home</i> </p>
<p>Opciones contempladas: </p>
<ol>
<li><b>Añadir reserva</b>: Para añadir una reserva <b>compruebo</b> que la reserva tenga una máxima duración de 2 horas (y no sea negativa ni 0) y que solo se pueda reservar con 7 dias de antelación como máximo
(y además no reservar en el pasado) en <b><i>reservas/create.blade.php</i></b>, estableciendo un max y min en los input y mediante código PHP calculando la fecha de hoy y de dentro de una semana. Compruebo que la hora no sea anterior a las 9:00 ni superior a las 21:00. También lo valido posteriormente con <i>Validator</i>.
<br/>Además, envío un <b>email</b> justo cuando se añade la reserva, con los datos de la misma (al usuario que ha creado dicha reserva)
<br/>También compruebo mediante consultas que no se puedan solapar las reservas al añadir ni editar.
</li>
<li><b>Ver, editar y borrar mis reserva</b>: En esta opción se acceden a las reservas del usuario que ha iniciado sesión y además permite borrarlas y editarlas. Para editarlas también
compruebo que la reserva sea válida (tanto su fecha como número de horas) igual que en <i>Añadir reserva</i></li>
<li><b>Buscar reservas por ID de usuario</b>: Se introduce un id y si el usuario no existe o no tiene reservas, se muestra como mensaje. Y si existe, se muestra la lista de sus reservas (accediendo a ellas con una consulta)</li>
<li><b>Buscar reservas por nombre de usuario</b>: 
Similar a buscar por ID pero he realizado una <b>subconsulta con join</b> para obtener las reservas 
de un usuario por su nombre. 
En la tabla reservas solo está el ID
del usuario que reservó, así que para obtener las reservas a partir
del nombre hace falta un join de ambas tablas (obtengo las reservas
donde el id del usuario en reservas sea igual al id del usuario en la
tabla user). 
Esta subconsulta está en <i>ReservasController</i>, en la función <b></i>index3</i></b>
</li>

<li><b>Buscar reservas por fecha</b>: 
Se buscan las reservas dada una fecha (que se comprueba que sea válida, no esté en el pasado ni sea superior a 7 días, ya que no pueden hacerse reservas con más de esta antelación). Esto lo realizo mediante una consulta.
Si no se encontrara ninguna reserva en la fecha especificada, se mostrará el mensaje de que no hay reservas en esa fecha.

<br/>El siguiente link de la documentación de Laravel me ha sido de gran 
ayuda para las consultas y subconsultas:
<i>https://laravel.com/docs/5.7/queries</i></li>


</ol>

<br/><b>API</b>
<p>Para la API solo se tiene que añadir un controlador muy similar al de la web (<b>ApiReservasController</b>) pero que devuelva un JSON con los datos en vez de redireccionar a las vistas y luego incluir en api.php las rutas.</p>
<p>Rutas creadas para llamar posteriormente en la aplicación android: </p>
<ol>
<li><i>https://laravel.marina.edufdezsoy.es/api/reservas </i>: Por <i>GET</i> para obtener todas las reservas del usuario actual </li>
<li><i>https://laravel.marina.edufdezsoy.es/api/reservas </i>: Por <i>POST</i> (pasándole numhoras, hora y fecha), para añadir una reserva</li>
<li><i> https://laravel.marina.edufdezsoy.es/api/reservas/14 </i>: Por <i>GET</i> para ver una reserva concreta (añadiendo /idreserva al final)</li>
<li><i>https://laravel.marina.edufdezsoy.es/api/reservas/14 </i>: Por <i>PUT</i> para modificar una reserva  (pasandole numhoras, hora y fecha y añadiendo el /idreserva en el link al final) <b></b></li>
<li><i> https://laravel.marina.edufdezsoy.es/api/reservas/14</i> : Por <i>DELETE</i> para borrar (añadiendo el /idreserva a borrar al final)</li>
<li><i> https://laravel.marina.edufdezsoy.es/api/reservas.index2</i> : Por <i>POST</i> (pasandole iduser) obtengo las reservas de ese user</li>
<li><i>https://laravel.marina.edufdezsoy.es/api/reservas.index3</i> : Por <i>POST</i> (pasandole name del usuario) obtengo las reservas de ese user. </li>
<li><i>https://laravel.marina.edufdezsoy.es/api/reservas.indexsearchdate</i> : Por <i>POST</i> (pasandole la fecha a buscar) obtengo las reservas que haya en esa fecha. </li>


</ol>