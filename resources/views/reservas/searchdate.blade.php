@extends('layouts.app')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Consultar reserva
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('reservas/indexsearchdate') }}">
          <div class="form-group">
              @csrf
              <label for="iduser">Fecha de la reserva:</label>
              <input type="date" class="form-control" name="fecha" required/>
          </div>
         
          <button type="submit" class="btn btn-primary">Buscar</button>
          
      </form>
  </div>
</div>
@endsection
