@extends('layouts.app')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Reservas </h2>
        </div>
        <div class="pull-right" align="right">
                <a class="btn btn-success mr-4 mb-2" href="{{ route('reservas.create') }}"> Nueva reserva </a>
        </div>
    </div>
</div>

<div class="uper">
  @if (session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br />
  @endif
  @if (session()->get('error'))
    <div class="alert alert-danger">
      {{ session()->get('error') }}  
    </div><br />
  @endif

  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Usuario</td>
          <td>Horas</td>
          <td>Fecha</td>
          <td>Hora</td>

        </tr>
    </thead>
    <tbody>
        @foreach($reservas as $reserva)
        <tr>
            <td>{{$reserva->id}}</td>
            <td>{{$reserva->user_id}}</td>
            <td>{{$reserva->numhoras}}</td>
            <td>{{$reserva->fecha}}</td>
            <td>{{$reserva->hora}}</td>

        </tr>
        @endforeach
    </tbody>
  </table>
<div>
@endsection
