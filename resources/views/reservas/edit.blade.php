@extends('layouts.app')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Editar reserva
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif


 <form method="post" action="{{ route('reservas.update', $reserva->id) }}">
        @method('PUT')
        @csrf
          <div class="form-group">
              @csrf
              <label for="fecha">Número de horas de la reserva sin editar: {{ $reserva->numhoras}}</label>      <br/>      
              <label for="numhoras">Número de horas nuevo:</label>
              <input  type="number" min="1" max="2" class="form-control" name="numhoras" required value={{ $reserva->numhoras}}/>
          </div>
          <div class="form-group">

<!--
              <label for="fecha">Fecha de la reserva sin editar: {{ $reserva->fecha}}</label>      <br/>   
-->   
			  <label for="fecha">Fecha de la reserva:</label>
              <input type="date" min=<?php echo date('Y-m-d'); ?> max=<?php echo date('Y-m-d', strtotime("+1 week")); ?> class="form-control" name="fecha" required value={{ $reserva->fecha}} />
          </div>

<!--
              <label for="fecha">Hora de la reserva sin editar: {{ $reserva->hora}}</label>      <br/>   
-->
			<div class="form-group">
			<label for="hora">Hora de la reserva:</label>
			<input value={{ $reserva->hora}} type="time" min=<?php echo "09:00:00";?> max=<?php echo "21:00:00";?> class="form-control" name="hora" required />
			</div>


          <div class="form-group">
              <label for="email">Email:</label>
              <input type="text" class="form-control" name="email" />
          </div>
          <button type="submit" class="btn btn-primary">Reservar</button>
          {{-- <a class="btn btn-secondary" href="{{ route('reservas') }}"> Cancelar</a> --}}
      </form>
  </div>
</div>
@endsection
