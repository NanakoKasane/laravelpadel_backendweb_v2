@extends('layouts.app')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Añadir reserva
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('reservas.store') }}">
          <div class="form-group">
              @csrf
              <label for="numhoras">Número de horas:</label>
              <input type="number" min="1" max="2" class="form-control" name="numhoras" required/>
          </div>
          <div class="form-group">
              <label for="fecha">Fecha de la reserva:</label>
              <input type="date" min=<?php echo date('Y-m-d'); ?> max=<?php echo date('Y-m-d', strtotime("+1 week")); ?> class="form-control" name="fecha" required />
          </div>

			<div class="form-group">
			<label for="hora">Hora de la reserva</label>
			<input type="time" min=<?php echo "09:00:00";?> max=<?php echo "21:00:00";?> class="form-control" name="hora" required />
			</div>

          <div class="form-group">
              <label for="email">Email:</label>
              <input type="text" class="form-control" name="email"/>
          </div>
          <button type="submit" class="btn btn-primary">Reservar</button>
          <a class="btn btn-secondary" href="{{ route('sites.index') }}"> Cancelar</a>
      </form>
  </div>
</div>
@endsection
