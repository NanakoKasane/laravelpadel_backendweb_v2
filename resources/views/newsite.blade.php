<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Nueva reserva en la pista de padel</title>
</head>
<body>
    <p>Estos son los de su reserva creada a las {{ $reserva->created_at }}:</p>
    <ul>
        <li>Horas: {{ $reserva->numhoras }}</li>
        <li>Fecha: {{ $reserva->fecha }}</li>
    </ul>
</body>
</html>
