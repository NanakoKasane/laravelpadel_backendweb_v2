@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">PADEL</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                <div class="content">
                    <div class="title m-b-md">
                        Marina Espinosa Gálvez - Ejercicio PADEL
                    </div>

                    <div class="links">
                        <a class="btn btn-info" href="{{ route('reservas.create') }}"> Añadir reserva</a> 
                       <a class="btn btn-info" href="{{ route('reservas.index') }}"> Ver y editar mis reservas</a> 
                       <a class="btn btn-success" href="{{ route('reservas.search') }}"> Buscar reservas por ID del usuario</a> 
                       <a class="btn btn-success" href="{{ route('reservas.searchname') }}"> Buscar reservas por nombre del usuario</a> 
                       <a class="btn btn-success" href="{{ route('reservas.searchdate') }}"> Buscar reservas en una fecha</a> 
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

                      {{-- <a class="btn btn-info" href="{{ route('reservas.search') }}"> Consultar reservas</a> --}}
