@extends('layouts.app')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Laravel 5.7 CRUD Example</h2>
        </div>
        <div class="pull-right" align="right">
                <a class="btn btn-success" href="{{ route('sites.create') }}"> Create new site</a>
        </div>
    </div>
</div>

<div class="uper">
  @if (session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br />
  @endif
  @if (session()->get('error'))
    <div class="alert alert-danger">
      {{ session()->get('error') }}  
    </div><br />
  @endif

  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Name</td>
          <td>Link</td>
          <td>Email</td>
          <td colspan="2">Action</td>
        </tr>
    </thead>
    <tbody>
        @foreach($sites as $site)
        <tr>
            <td>{{$site->id}}</td>
            <td>{{$site->name}}</td>
            <td>{{$site->link}}</td>
            <td>{{$site->email}}</td>
            <td><a href="{{ route('sites.edit',$site->id)}}" class="btn btn-primary">Edit</a></td>
            <td>
                <form action="{{ route('sites.destroy', $site->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
@endsection
